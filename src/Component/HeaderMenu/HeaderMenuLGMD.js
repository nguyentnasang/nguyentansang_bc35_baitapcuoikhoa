import React, { useEffect, useState } from "react";
import { DownOutlined } from "@ant-design/icons";
import { Button, Dropdown, message, Space } from "antd";
import { getportCourse } from "../../service/courseService";
import Navbar from "./Navbar";
import SearchCourse from "./SearchCourse";
import EventPage from "./../../Pages/EventPage/EventPage";
export default function HeaderMenuLGMD() {
  const onClick = ({ key }) => {
    // <NavLink to={`/danhmuckhoahoc`} />;
    window.location.href = `/danhmuckhoahoc/${key}`;
  };
  const [portCourseArr, setPortCourseArr] = useState([]);
  useEffect(() => {
    getportCourse()
      .then((res) => {
        setPortCourseArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderDanhMucKhoaHoc = () => {
    return portCourseArr.map((item) => {
      return {
        label: (
          <p className="text-cyan-900 font-medium hover:text-green-600">
            {item.tenDanhMuc}
          </p>
        ),
        key: item.maDanhMuc,
      };
    });
  };
  const items = renderDanhMucKhoaHoc();

  return (
    <div className="text-cyan-900 font-medium py-1">
      <div className="flex justify-between items-center h-full">
        <a className="flex justify-center items-center" href="/">
          <img width={100} src={require("../../img/elearning.jpg")} alt="" />
          <div className="text-left">
            <p
              style={{ letterSpacing: "0.5rem" }}
              className=" font-black text-green-600 lg:text-3xl md:text-2xl"
            >
              Elearning
            </p>
            <p
              style={{ wordSpacing: "0.3rem" }}
              className=" text-green-600 lg:text-sm md:text-xs"
            >
              Đào tạo chuyên gia lập trình
            </p>
          </div>
        </a>
        {/* <div
          className="block md:block lg:block "
          onClick={() => {
            window.location.href = "/findcourse";
          }}
        >
          <SearchCourse />
        </div> */}
        <ul className="flex space-x-5 lg:space-x-10 md:space-x-2 font-bold">
          <li>
            {" "}
            <Dropdown
              menu={{
                items,
                onClick,
              }}
            >
              <a
                className="hover:text-green-600"
                onClick={(e) => e.preventDefault()}
              >
                <Space>
                  <p> Danh Mục</p>
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
          </li>
          <li>
            <a className="hover:text-green-600" href="/listcourse">
              Khóa Học
            </a>
          </li>
          <li className="hover:text-green-600">
            <a href="/findcourse">Tìm Kiếm</a>
          </li>
          <li className="hover:text-green-600">
            <a href="/EventPage">Sự Kiện</a>
          </li>
          <li className="hover:text-green-600">
            <a href="/infopage">Thông Tin</a>
          </li>
        </ul>
        <Navbar />
      </div>
    </div>
  );
}
