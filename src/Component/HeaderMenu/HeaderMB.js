import React, { useEffect, useState } from "react";
import { MenuOutlined } from "@ant-design/icons";
import { Dropdown, Space, Button } from "antd";
import { getportCourse } from "../../service/courseService";
import Navbar from "./Navbar";
export default function HeaderMB() {
  const [CourseArr, setCourseArr] = useState([]);
  useEffect(() => {
    getportCourse()
      .then((res) => {
        setCourseArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderCourse = () => {
    return CourseArr.map((item) => {
      return {
        key: "1",
        label: (
          <a
            className="hover:text-green-600"
            onClick={() => {
              window.location.href = `/danhmuckhoahoc/${item.maDanhMuc}`;
            }}
          >
            {item.tenDanhMuc}
          </a>
        ),
      };
    });
  };
  const items = [
    {
      key: "1",
      type: "group",
      // label: "Group title",
      children: [
        {
          key: "3",
          label: (
            <a href="/">
              <p className="hover:text-green-600"> Trang chủ</p>
            </a>
          ),
        },
        {
          key: "1",
          label: (
            <a className="hover:text-green-600" href="">
              Danh mục
            </a>
          ),
          children: renderCourse(),
        },
        {
          key: "2",
          label: (
            <a className="hover:text-green-600" href="/listcourse">
              {" "}
              Khóa học
            </a>
          ),
        },
        {
          key: "3",
          label: <a href="/findcourse">Tìm Kiếm</a>,
        },
        {
          key: "4",
          label: <a href="/EventPage">Sự Kiện</a>,
        },
        {
          key: "5",
          label: <a href="/infopage">Thông Tin</a>,
        },
      ],
    },
  ];

  return (
    <div className="flex justify-between items-center">
      <Dropdown
        menu={{
          items,
        }}
      >
        <a onClick={(e) => e.preventDefault()}>
          <Space>
            <MenuOutlined className="text-xl" />
          </Space>
        </a>
      </Dropdown>
      <Navbar />
    </div>
  );
}
