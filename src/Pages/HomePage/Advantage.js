import React from "react";
import {
  ClusterOutlined,
  DeploymentUnitOutlined,
  KeyOutlined,
  RedditOutlined,
  ToolOutlined,
  UsergroupAddOutlined,
} from "@ant-design/icons";
import { Bounce, Slide } from "react-awesome-reveal";
export default function Advantage() {
  return (
    <div className="px-10 my-10 lg:px-20 md:px20">
      <Bounce triggerOnce direction="left" duration={2000}>
        <div className="text-right mb-10">
          <h2 className="text-5xl tracking-widest font-bold  text-green-500">
            Điểm ưu việt
          </h2>
          <p className="text-2xl">CHỈ CÓ TẠI ELEARNING</p>
        </div>
      </Bounce>
      <div className="block grid-cols-3 gap-5 lg:grid md:grid">
        <Slide duration={2060} direction="right" triggerOnce>
          <div className="mt-3 block grid-cols-5 text-left lg:grid">
            <div className=" col-span-1 w-14 h-14 border-2 border-green-600 flex justify-center items-center rounded-full text-green-700 hover:bg-green-600 hover:text-white  text-2xl">
              <ClusterOutlined />
            </div>
            <div className="col-span-4">
              <h2 className="font-bold text-2xl">
                Học theo lộ trình, có định hướng
              </h2>
              <p className="mt-5">
                Elearning sẽ định hướng và đưa ra các lộ trình học lập trình
                nhằm phát triển năng lực và niềm đam mê lập trình của bạn để có
                việc ngay sau học.
              </p>
            </div>
          </div>
        </Slide>
        <Slide duration={2050} direction="right" triggerOnce>
          <div className="mt-3 block grid-cols-5 text-left lg:grid">
            <div className=" col-span-1 w-14 h-14 border-2 border-green-600 flex justify-center items-center rounded-full text-green-700 hover:bg-green-600 hover:text-white text-2xl">
              <DeploymentUnitOutlined />
            </div>
            <div className="col-span-4">
              <h2 className="font-bold text-2xl">
                Nền tảng, tư duy, cốt lõi trong lập trình
              </h2>
              <p className="mt-5">
                Elearning cung cấp những nền tảng, giá trị tư duy cốt lõi nhất
                trong lập trình. Bạn sẽ tự tin trước sự thay đổi của công nghệ
                và môi trường làm việc.
              </p>
            </div>
          </div>
        </Slide>
        <Slide duration={2040} direction="right" triggerOnce>
          <div className="mt-3 block grid-cols-5 text-left lg:grid">
            <div className=" col-span-1 w-14 h-14 border-2 border-green-600 flex justify-center items-center rounded-full text-green-700 hover:bg-green-600 hover:text-white text-2xl">
              <ToolOutlined />
            </div>
            <div className="col-span-4">
              <h2 className="font-bold text-2xl">
                Mài giũa bạn qua kinh nghiệm, dự án thực tế
              </h2>
              <p className="mt-5">
                Đội ngũ Giảng viên và các Mentor là những người dày dạn kinh
                nghiệm qua các dự án thực tế tại các công ty lớn sẽ truyền đạt
                những kinh nghiệm "máu lửa" cho bạn.
              </p>
            </div>
          </div>
        </Slide>
        <Slide duration={2030} direction="right" triggerOnce>
          <div className="mt-3 block grid-cols-5 text-left lg:grid">
            <div className=" col-span-1 w-14 h-14 border-2 border-green-600 flex justify-center items-center rounded-full text-green-700 hover:bg-green-600 hover:text-white text-2xl">
              <UsergroupAddOutlined />
            </div>
            <div className="col-span-4">
              <h2 className="font-bold text-2xl">
                Teamwork, Scrum - Agile. Mentor tận tâm
              </h2>
              <p className="mt-5">
                Bạn sẽ được giao dự án và làm theo Teamwork ngay từ ngày đầu
                tiên. Đóng vai trò một thành viên trong qui trình Scrum, Agile.
                Được Mentor hỗ trợ tân tâm, nhiệt tình.
              </p>
            </div>
          </div>
        </Slide>
        <Slide duration={2020} direction="right" triggerOnce>
          <div className="mt-3 block grid-cols-5 text-left lg:grid">
            <div className=" col-span-1 w-14 h-14 border-2 border-green-600 flex justify-center items-center rounded-full text-green-700 hover:bg-green-600 hover:text-white text-2xl">
              <RedditOutlined />
            </div>
            <div className="col-span-4">
              <h2 className="font-bold text-2xl">
                Công nghệ mới, chuyên sâu, thực tế
              </h2>
              <p className="mt-5">
                Bạn được học và trải nghiệm các công nghệ lập trình mới nhất,
                chuyên sâu, bám sát nhu cầu tuyển dụng thực tế từ doanh nghiệp.
              </p>
            </div>
          </div>
        </Slide>
        <Slide duration={2010} direction="right" triggerOnce>
          <div className="mt-3 block grid-cols-5 text-left lg:grid">
            <div className=" col-span-1 w-14 h-14 border-2 border-green-600 flex justify-center items-center rounded-full text-green-700 hover:bg-green-600 hover:text-white text-2xl">
              <KeyOutlined />
            </div>
            <div className="col-span-4">
              <h2 className="font-bold text-2xl">
                Trao tay chìa khóa thành công toàn diện
              </h2>
              <p className="mt-5">
                Hướng dẫn viết CV, phỏng vấn. Kết nối doanh nghiệp, gặp gỡ doanh
                nghiệp, phỏng vấn cùng doanh nghiệp ngay sau khi tốt nghiệp.
              </p>
            </div>
          </div>
        </Slide>
      </div>
    </div>
  );
}
