import React, { useEffect, useState } from "react";
import { message, Space, Table, Tag } from "antd";
import {
  deleteCourse,
  getCourse,
  getFindCourse,
} from "./../../../service/courseService";
import { useNavigate } from "react-router-dom";
import Search from "antd/es/input/Search";
import { useDispatch, useSelector } from "react-redux";
import { setCourseFind } from "../../../redux-toolkit/userSlice";
export default function CourseListPage() {
  const columns = [
    {
      title: "mã khóa học",
      dataIndex: "maKhoaHoc",
      key: "namaKhoaHoc",
    },
    {
      title: "tên khóa học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "hình ảnh",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
    },
    {
      title: "người tạo",
      dataIndex: "nguoiTao",
      key: "nguoiTao",
    },
    {
      title: "thao tác",
      dataIndex: "thaoTac",
      key: "thaoTac",
    },
  ];

  const [courseArr, setcourseArr] = useState([]);
  const navigate = useNavigate();
  const renderListCourse = () => {
    return courseArr.map((item) => {
      return {
        ...item,
        hinhAnh: <img className="w-40" src={item.hinhAnh} alt="" />,
        nguoiTao: item.nguoiTao.hoTen,
        thaoTac: (
          <div>
            <button
              onClick={() => {
                navigate(`/admin/mrgregisuser/${item.maKhoaHoc}`);
              }}
              className="border-2 border-black m-2 rounded-md px-2 py-1"
            >
              ghi danh
            </button>
            <button
              onClick={() => {
                message.warning("tính năng đang phát triển");
              }}
              className="border-2 border-black m-2 rounded-md px-2 py-1"
            >
              sửa
            </button>
            <button
              onClick={() => {
                deleteCourse(item.maKhoaHoc)
                  .then((res) => {
                    message.success("xóa thành công");
                    setTimeout(() => {
                      window.location.reload();
                    }, 1000);
                  })
                  .catch((err) => {
                    message.error("xóa thất bại");
                  });
              }}
              className="border-2 border-black m-2 rounded-md px-2 py-1"
            >
              xóa
            </button>
          </div>
        ),
      };
    });
  };

  useEffect(() => {
    getCourse()
      .then((res) => {
        setcourseArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const newUserArrFind = () => {
    return pennant?.map((item) => {
      return {
        ...item,
        hinhAnh: <img className="w-40" src={item.hinhAnh} alt="" />,
        nguoiTao: item.nguoiTao.hoTen,
        thaoTac: (
          <div>
            <button
              onClick={() => {
                navigate(`/admin/mrgregisuser/${item.maKhoaHoc}`);
              }}
              className="border-2 border-black m-2 rounded-md px-2 py-1"
            >
              ghi danh
            </button>
            <button className="border-2 border-black m-2 rounded-md px-2 py-1">
              sửa
            </button>
            <button
              onClick={() => {
                deleteCourse(item.maKhoaHoc)
                  .then((res) => {
                    message.success("xóa thành công");
                    setTimeout(() => {
                      window.location.reload();
                    }, 1000);
                  })
                  .catch((err) => {
                    message.error("xóa thất bại");
                  });
              }}
              className="border-2 border-black m-2 rounded-md px-2 py-1"
            >
              xóa
            </button>
          </div>
        ),
      };
    });
  };
  let dispatch = useDispatch();

  const onSearch = (value) => {
    getFindCourse(value)
      .then((res) => {
        dispatch(setCourseFind(res.data));
      })
      .catch((err) => {
        message.error("Không tìm thấy khóa học");
      });
  };
  let pennant = useSelector((state) => {
    return state.userSlice.courseFind;
  });
  let renderUser = () => {
    if (pennant.length > 0) {
      return newUserArrFind();
    } else {
      return renderListCourse();
    }
  };
  return (
    <div className="text-left">
      <a
        className="text-2xl underline font-semibold"
        href={`/admin/mgrformcourse`}
      >
        THÊM KHÓA HỌC
      </a>{" "}
      <Search
        className="bg-green-500 mt-10"
        placeholder="Tìm kiếm khóa học"
        allowClear
        enterButton="tìm kiếm"
        size="large"
        onSearch={onSearch}
      />
      <Table columns={columns} dataSource={renderUser()} />;
    </div>
  );
}
