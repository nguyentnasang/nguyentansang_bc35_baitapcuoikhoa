import React, { useEffect, useState } from "react";
import { getCourse } from "../../service/courseService";
import { Button, Card, message } from "antd";
import { NavLink } from "react-router-dom";
import { postRegisCourse } from "./../../service/courseService";
import { useSelector } from "react-redux";
export default function ListCoursePage() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  console.log("user", user);
  const [courseArr, setCourseArr] = useState([]);
  useEffect(() => {
    getCourse()
      .then((res) => {
        setCourseArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderCardCourse = () => {
    return courseArr.map((item) => {
      return (
        <Card
          className="shadow-2xl"
          hoverable
          cover={
            <img
              className="h-48 object-cover"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <p className="text-xl font-medium">{item.tenKhoaHoc}</p>

          <div>
            <div className="my-5">
              <p className="line-through italic">800.000đ</p>
              <p className="text-xl text-green-500 font-medium">400.000đ</p>
            </div>
            <NavLink to={`/detail/${item.maKhoaHoc}`}>
              <button className="bg-green-400 mx-2 my-1 px-2 py-1 rounded-md text-white font-medium hover:brightness-75">
                Xem chi tiết
              </button>
            </NavLink>
            <button
              className="bg-green-400 mx-2 my-1 px-2 py-1 rounded-md text-white font-medium hover:brightness-75"
              onClick={() => {
                let axiosArr = {
                  maKhoaHoc: item.maKhoaHoc,
                  taiKhoan: user?.taiKhoan,
                };
                postRegisCourse(axiosArr)
                  .then((res) => {
                    console.log(res);
                    message.success("Đăng ký thành công");
                  })
                  .catch((err) => {
                    console.log(err.response.data);
                    if (user == null) {
                      message.warning("vui lòng đăng nhập để thực hiện");
                    } else {
                      message.warning(err.response.data);
                    }
                  });
              }}
            >
              Đăng ký
            </button>
          </div>
        </Card>
      );
    });
  };
  return (
    <div className="pt-32">
      <h2 className="text-3xl font-bold text-green-700">
        Các Khóa Học Phổ Biến Tại Elearning
      </h2>
      <div className="  gap-5 px-20 mt-24 lg:grid lg:grid-cols-4 md:grid md:grid-cols-3">
        {renderCardCourse()}
      </div>
    </div>
  );
}
