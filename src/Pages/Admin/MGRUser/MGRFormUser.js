import React, { useEffect, useState } from "react";
import { Button, Form, Input, message, Select } from "antd";
import { Option } from "antd/es/mentions";
import {
  getFindUser,
  postAddUser,
  putUserInfo,
} from "../../../service/userService";
import { LeftOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import { userLocalStoreChange } from "../../../service/localService";
export default function MGRFormUser() {
  let param = useParams();
  let pennant = true;
  // let UserChange = userLocalStoreChange.get();
  const [UserChange, setUserChange] = useState([]);
  console.log("UserChange", UserChange);
  useEffect(() => {
    // chỗ này bị lỗi chỉ hiển thị thông tin đăng nhập!!!!!!!!
    getFindUser(param.tk)
      .then((res) => {
        console.log(res);
        console.log("res", res);
        setUserChange(res.data);
        // userLocalStoreChange.set(res.data);
        if (UserChange[0]?.taiKhoan !== param.tk) {
          window.location.reload();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onFinish = (values) => {
    // console.log("Success:", values);
    if (pennant) {
      postAddUser(values)
        .then((res) => {
          message.success("thêm người dùng thành công");

          console.log(res);
        })
        .catch((err) => {
          message.error(err.response.data);
          console.log(err.response.data);
        });
    } else {
      putUserInfo(values)
        .then((res) => {
          message.success("cập nhật thành công");

          userLocalStoreChange.set(res.data);

          console.log(res);
        })
        .catch((err) => {
          message.error("cập nhật thất bại");
          console.log(err);
        });
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      {" "}
      <Form
        fields={[
          {
            name: ["taiKhoan"],
            value: UserChange[0].taiKhoan,
          },
          {
            name: ["matKhau"],
            value: UserChange[0].matKhau,
          },
          {
            name: ["email"],
            value: UserChange[0].email,
          },
          {
            name: ["soDT"],
            value: UserChange[0].soDt,
          },
          {
            name: ["maNhom"],
            value: "GP01",
          },
          {
            name: ["maLoaiNguoiDung"],
            value: UserChange[0].maLoaiNguoiDung,
          },
          {
            name: ["hoTen"],
            value: UserChange[0].hoTen,
          },
        ]}
        className=""
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="tài Khoản"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="mật khẩu"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="họ tên"
          name="hoTen"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="số điện thoại"
          name="soDT"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="maLoaiNguoiDung"
          label="mã loại người dùng"
          rules={[{ required: true }]}
        >
          <Select
            placeholder="chọn loại người dùng"
            // onChange={onGenderChange}
            allowClear
          >
            <Option value="HV">Học Viên</Option>
            <Option value="GV">Giáo Vụ</Option>
          </Select>
        </Form.Item>
        <Form.Item name="maNhom" label="mã nhóm" rules={[{ required: true }]}>
          <Select
            placeholder="chọn nhóm người dùng"
            // onChange={onGenderChange}
            allowClear
          >
            <Option value="GP06">nhóm 6</Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="email"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        ></Form.Item>
        <a className="flex justify-center items-center" href="/admin">
          <LeftOutlined />
          <LeftOutlined />
          trở lại
        </a>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            onClick={() => {
              pennant = true;
            }}
            className="bg-green-500"
            htmlType="submit"
          >
            Thêm
          </Button>
          <Button
            onClick={() => {
              pennant = false;
            }}
            className="bg-green-500"
            htmlType="submit"
          >
            Cập Nhật
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
// "taiKhoan": "string",
//   "matKhau": "string",
//   "hoTen": "string",
//   "soDT": "string",
//   "maLoaiNguoiDung": "string",
//   "maNhom": "string",
//   "email": "string"
