import { ClockCircleOutlined, PlayCircleOutlined } from "@ant-design/icons";
import React from "react";

export default function ContentCourse() {
  return (
    <div className="mt-5">
      <p className="text-2xl font-bold">NỘI DUNG KHÓA HỌC</p>
      <p className="text-xl font-bold my-2">MỤC 1: GIỚI THIỆU </p>
      <h1 className="text-xl">bài học:</h1>
      <p className="flex justify-between items-center my-1 ">
        <div>
          <span className="">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Các khái niệm về React Component
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-center my-1 ">
        <div>
          <span className="">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Thiết lập môi trường cho Windows
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-center my-1 ">
        <div>
          <span className="">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Tạo ứng dụng React - React-Scripts
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-center my-1 ">
        <div>
          <span className="">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Ghi chú nhanh về dấu ngoặc kép cho string interpolation
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="text-xl font-bold my-2">MỤC 2: KIẾN THỨC CĂN BẢN</p>
      <h1 className="text-xl">bài học:</h1>
      <p className="flex justify-between items-cente my-1r">
        <div>
          <span className=" ">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Trang chủ và thành phần thư mục
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-center my-1 ">
        <div>
          <span className="">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Hướng dẫn khóa học + Liên kết Github
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-cente my-1r">
        <div>
          <span className=" ">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Trang chủ thương mại điện tử + thiết lập SASS
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-cente my-1r">
        <div>
          <span className=" ">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Tệp CSS và SCSS
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-cente my-1r">
        <div>
          <span className=" ">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          React 17: Cập nhật các gói + Phiên bản React mới nhất
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="text-xl font-bold my-2">MỤC 3: KIẾN THỨC CHUYÊN SÂU</p>
      <h1 className="text-xl">bài học:</h1>
      <p className="flex justify-between items-cente my-1r">
        <div>
          <span className=" ">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          connect() and mapStateToProps
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-center my-1 ">
        <div>
          <span className="">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Trạng thái thư mục vào Redux
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
      <p className="flex justify-between items-cente my-1r">
        <div>
          <span className=" ">
            <PlayCircleOutlined className=" text-green-700 text-2xl" />
          </span>
          Thành phần Tổng quan về Bộ sưu tập
        </div>
        <span className="flex justify-end items-center ">
          <ClockCircleOutlined className="text-green-700 text-2xl mr-2" />
          <p>11:29</p>
        </span>
      </p>
    </div>
  );
}
