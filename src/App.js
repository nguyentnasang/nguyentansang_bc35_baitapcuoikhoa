import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./HOC/Layout";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import BackendPage from "./Pages/Course/CoursePage";
import CoursePage from "./Pages/Course/CoursePage";
import DetaiPage from "./Pages/DetailPage/DetaiPage";
import RegisPage from "./Pages/RegisPage/RegisPage";
import ListCoursePage from "./Pages/ListCoursePage/ListCourcePage";
import SearchCourse from "./Component/HeaderMenu/SearchCourse";
import FindCourse from "./Pages/FindCourse/FindCourse";
import UserInfo from "./Pages/UserInfo/UserInfo";
import Admin from "./Pages/Admin/Admin";
import MGRegisCourse from "./Pages/Admin/MGRUser/MGRegisCourse";
import MGRFormUserUpdate from "./Pages/Admin/MGRUser/MGRFormUserUpdate";
import MGRFormCourse from "./Pages/Admin/MGRCourse/MGRFormCourse";
import MGRRegisUser from "./Pages/Admin/MGRCourse/MGRRegisUser";
import Loading from "./Component/Loading/Loading";
import EventPage from "./Pages/EventPage/EventPage";
import InfoPage from "./Pages/InfoPage/InfoPage";
import MGRFormUserNew from "./Pages/Admin/MGRUser/MGRFormUserNew";

function App() {
  return (
    <div className="App font-libree">
      <Loading />
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />

          <Route
            path="/danhmuckhoahoc/:id"
            element={
              <Layout>
                <CoursePage />
              </Layout>
            }
          />
          <Route
            path="/detail/:id"
            element={
              <Layout>
                <DetaiPage />
              </Layout>
            }
          />
          <Route
            path="/listcourse"
            element={
              <Layout>
                <ListCoursePage />
              </Layout>
            }
          />
          <Route
            path="/findcourse"
            element={
              <Layout>
                <FindCourse />
              </Layout>
            }
          />
          <Route
            path="/userinfo"
            element={
              <Layout>
                <UserInfo />
              </Layout>
            }
          />
          <Route
            path="/EventPage"
            element={
              <Layout>
                <EventPage />
              </Layout>
            }
          />
          <Route
            path="/infopage"
            element={
              <Layout>
                <InfoPage />
              </Layout>
            }
          />
          <Route path="/admin/mrgregiscourse/:tk" element={<MGRegisCourse />} />
          <Route path="/admin/mrgregisuser/:tk" element={<MGRRegisUser />} />
          <Route
            path="/admin/mgrformuserupdate/:tk"
            element={<MGRFormUserUpdate />}
          />
          <Route path="/admin/mgrformusernew" element={<MGRFormUserNew />} />
          <Route path="/admin/mgrformcourse" element={<MGRFormCourse />} />

          <Route path="/admin" element={<Admin />} />
          <Route path="/regis" element={<RegisPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
