import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getDetailCourse, postRegisCourse } from "../../service/courseService";
import ContentCourse from "./ContentCourse";
import {
  CheckOutlined,
  DollarCircleOutlined,
  EnvironmentOutlined,
  FieldTimeOutlined,
  FileProtectOutlined,
  FormOutlined,
  MonitorOutlined,
  PullRequestOutlined,
  QrcodeOutlined,
  RiseOutlined,
  TableOutlined,
  ToolOutlined,
  UsergroupAddOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import Suggest from "../HomePage/Suggest";
import { useSelector } from "react-redux";
import { message } from "antd";
export default function DetaiPage() {
  let params = useParams();
  const [userArr, setuserArr] = useState([]);
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  let { tenKhoaHoc, hinhAnh, maKhoaHoc } = userArr;
  useEffect(() => {
    getDetailCourse(params.id)
      .then((res) => {
        setuserArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const axiosArr = {
    maKhoaHoc: maKhoaHoc,
    taiKhoan: user?.taiKhoan,
  };
  return (
    <div className=" pt-24 ">
      <div className="grid grid-cols-3 pt-16">
        <div className="col-span-2 text-left mx-7">
          <p className="text-3xl">
            Tên Khóa Học <p className="font-bold">{tenKhoaHoc}</p>
          </p>
          <p className="italic"> (OFFLINE + ONLINE TƯƠNG TÁC CÓ RECORD)</p>
          <p className="my-7 text-justify">
            Đã có hơn <b>6200</b> bạn đăng kí học và có việc làm thông qua
            chương trình đào tạo Bootcamp Lập trình Front End chuyên nghiệp từ
            Zero tại Elearning. Khóa học <b>100% thực hành</b> cường độ cao theo
            <b>dự án thực tế</b> và kết nối doanh nghiệp hỗ trợ{" "}
            <b>tìm việc ngay</b> sau khi học. Phương pháp đào tạo nghề chuẩn đại
            học Arizona - ASU Mỹ - tập trung tư duy, phân tích bài toán giúp cho
            học viên dễ dàng phát triển từ dev lên senior, leader và làm việc
            tại bất kì môi trường nào.
          </p>
          <div className="grid grid-cols-2">
            <div>
              <p className="flex justify-start items-center text-green-700 font-bold">
                <UserOutlined />
                AI CÓ THỂ THAM GIA ?
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Mới học lập trình, thiếu định hướng & lộ trình học tập
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Trái ngành học lập trình, chuyển nghề
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Hoàn thành chương trình trung học phổ thông (12/12)
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Yếu tư duy lập trình, mất gốc muốn học để xin việc làm
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Chủ dự án muốn quản lý team dev, startup muốn hiểu rõ việc làm
                của dev
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Thêm nghề để kiếm thêm thu nhập ngoài giờ
              </p>
            </div>
            <div>
              <p className="flex justify-start items-center text-green-700 font-bold">
                <EnvironmentOutlined /> HỌC XONG LÀM VIỆC TẠI ĐÂU ?
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Apply vào tất cả công ty tuyển dụng Front End Dev ở vị trí
                fresher hoặc juinor
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Các công ty outsourcing - gia công phần mềm
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Các công ty startup - khởi nghiệp
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Công ty, tập đoàn trong nước và nước ngoài...
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Có thể apply ngay vào Fresher, Junior với mức lương khởi điểm từ
                7tr5 đến 15tr
              </p>
              <p className="mt-2">
                <CheckOutlined className="text-xl mr-2 font-black" />
                Nhận các job freelancer về xây dựng front end cho website
              </p>
            </div>
          </div>
          <div className="my-7">
            <p className="text-2xl font-bold">Đầu ra khóa học</p>
            <p className="mt-3 flex justify-start items-center">
              <p className="mr-5 text-xl text-green-700">
                <ToolOutlined />
              </p>
              Bạn sẽ đạt được kỹ năng của một lập trình Front-End chuyên nghiệp
            </p>
            <p className="mt-3 flex justify-start items-center">
              <p className="mr-5 text-xl text-green-700">
                <TableOutlined />
              </p>
              Xây dựng và triển khai được các dự án thực tế về React JS, tương
              tác Backend, Servies, Web API, JSON
            </p>
            <p className="mt-3 flex justify-start items-center">
              <p className="mr-5 text-xl text-green-700">
                <PullRequestOutlined />
              </p>
              Vừa chuyên nghiệp hóa chuyên môn, vừa thông thạo các kĩ năng mềm
              trong làm việc
            </p>
            <p className="mt-3 flex justify-start items-center">
              <p className="mr-5 text-xl text-green-700">
                <QrcodeOutlined />
              </p>
              Được rèn luyện tư duy, logic nền tảng, cốt lõi trong nghề lập
              trình
            </p>
            <p className="mt-3 flex justify-start items-center">
              <p className="mr-5 text-xl text-green-700">
                <FileProtectOutlined />
              </p>
              Cấp chứng nhận hoàn thành khóa học sau khi hoàn thành các yều cầu
              của khóa học
            </p>
            <p className="mt-3 flex justify-start items-center">
              <p className="mr-5 text-xl text-green-700">
                <MonitorOutlined />
              </p>
              Hỗ trợ tìm việc trọn đời qua hệ thống tìm việc
            </p>
          </div>
          <ContentCourse />
        </div>
        <div className="shadow-2xl border-2 p-5">
          <img className="w-full" src={hinhAnh} alt="" />
          <div className="px-5">
            <div className="flex justify-center items-center text-2xl font-semibold my-5">
              <p className="flex justify-center items-center text-yellow-500 mr-2">
                <DollarCircleOutlined />
              </p>
              <p className="flex justify-center items-center">400.000NVĐ</p>
            </div>
            <button
              onClick={() => {
                postRegisCourse(axiosArr)
                  .then((res) => {
                    console.log(res);
                    message.success("Đăng ký thành công");
                  })
                  .catch((err) => {
                    console.log(err);
                    if (user == null) {
                      message.warning("vui lòng đăng nhập để thực hiện");
                    } else {
                      message.warning(err.request.response);
                    }
                  });
              }}
              className="text-xl border-2 border-green-500 text-green-500 w-full py-1 hover:bg-green-500 hover:text-white"
            >
              Đăng Ký
            </button>

            <div className="flex justify-between items-center text-xl mt-10">
              <p>
                Ghi Danh: <b>23 Học viên</b>
              </p>
              <p className="text-yellow-500">
                <UsergroupAddOutlined />
              </p>
            </div>
            <div className="flex justify-between items-center text-xl mt-10">
              <p>
                Thời gian: <b>20 giờ</b>
              </p>
              <p className="text-yellow-500">
                <FieldTimeOutlined />
              </p>
            </div>
            <div className="flex justify-between items-center text-xl mt-10">
              <p>
                Bài học: <b>23</b>
              </p>
              <p className="text-yellow-500">
                <FormOutlined />
              </p>
            </div>
            <div className="flex justify-between items-center text-xl mt-10">
              <p>
                Video: <b>23</b>
              </p>
              <p className="text-yellow-500">
                <VideoCameraOutlined />
              </p>
            </div>
            <div className="flex justify-between items-center text-xl mt-10">
              <p>
                Trình độ: <b>Người mới bắt đầu</b>
              </p>
              <p className="text-yellow-500">
                <RiseOutlined />
              </p>
            </div>
          </div>
        </div>
      </div>
      <Suggest />
    </div>
  );
}
