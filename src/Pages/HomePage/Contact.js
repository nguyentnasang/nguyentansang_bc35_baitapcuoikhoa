import React from "react";
import { Button, Form, Input, message } from "antd";
import { Bounce, Slide } from "react-awesome-reveal";
const MyFormItemContext = React.createContext([]);
function toArr(str) {
  return Array.isArray(str) ? str : [str];
}
const MyFormItemGroup = ({ prefix, children }) => {
  const prefixPath = React.useContext(MyFormItemContext);
  const concatPath = React.useMemo(
    () => [...prefixPath, ...toArr(prefix)],
    [prefixPath, prefix]
  );
  return (
    <MyFormItemContext.Provider value={concatPath}>
      {children}
    </MyFormItemContext.Provider>
  );
};
const MyFormItem = ({ name, ...props }) => {
  const prefixPath = React.useContext(MyFormItemContext);
  const concatName =
    name !== undefined ? [...prefixPath, ...toArr(name)] : undefined;
  return <Form.Item name={concatName} {...props} />;
};
export default function Contact() {
  const onFinish = (value) => {};
  return (
    <div
      id="lienhe"
      className=" grid-cols-2 bg-gradient-to-r from-cyan-500 to-green-500 lg:grid md:grid"
    >
      <Slide duration={2000} direction="left" triggerOnce>
        <img
          className="hidden lg:block md:block"
          src={require("../../img/contact.jpg")}
          alt=""
        />
      </Slide>
      <Slide duration={2000} direction="right" triggerOnce>
        <div className="flex flex-col justify-center items-center p-5 py-14 lg:p-20 md:p-5 ">
          <Form
            style={{ borderRadius: "20px" }}
            className="w-full bg-white p-20 bg-opacity-90"
            name="form_item_path"
            layout="vertical"
            onFinish={onFinish}
          >
            <Bounce delay={1800} triggerOnce duration={1500}>
              <p className="text-3xl font-bold text-green-700 mb-5">
                LIÊN HỆ TƯ VẤN
              </p>
            </Bounce>
            <MyFormItemGroup prefix={["user"]}>
              <MyFormItemGroup prefix={["name"]}>
                <MyFormItem name="firstName" label="Họ và tên">
                  <Input />
                </MyFormItem>
                <MyFormItem name="email" label="Email liên hệ">
                  <Input />
                </MyFormItem>
              </MyFormItemGroup>

              <MyFormItem name="soDT" label="Điện thoại liên hệ">
                <Input />
              </MyFormItem>
              <MyFormItem name="mess">
                <Input
                  className="mt-3"
                  placeholder="Bạn cần tư vấn hãy để lại lời nhắn tại đây..."
                />
              </MyFormItem>
            </MyFormItemGroup>

            <button
              onClick={() => {
                message.success("Đăng ký thành công");
              }}
              className="bg-green-500 text-yellow-50 px-2 py-3 rounded-md hover:brightness-75"
              htmlType="submit"
            >
              ĐĂNG KÝ TƯ VẤN
            </button>
          </Form>
        </div>
      </Slide>
    </div>
  );
}
