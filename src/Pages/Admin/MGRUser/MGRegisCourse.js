import React from "react";
import { useParams } from "react-router-dom";
import MGRNotRegisCourse from "./MGRNotRegisCourse";
import MGRReadyCourse from "./MGRReadyCourse";
import MGRWaitCourse from "./MGRWaitCourse";

export default function MGRegisCourse() {
  let param = useParams();

  return (
    <div>
      <MGRNotRegisCourse />
      <MGRWaitCourse />
      <MGRReadyCourse />
    </div>
  );
}
