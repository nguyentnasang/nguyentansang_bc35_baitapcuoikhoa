import React from "react";
import { Tabs } from "antd";
import { Card } from "antd";
import { Bounce } from "react-awesome-reveal";
const { Meta } = Card;
const onChange = (key) => {
  console.log(key);
};
function slowScroll(destination, duration) {
  let start = window.pageYOffset;
  let startTime = null;

  function animation(currentTime) {
    if (startTime === null) startTime = currentTime;
    let timeElapsed = currentTime - startTime;
    let run = easeInOutQuad(timeElapsed, start, destination, duration);
    window.scrollTo(0, run);
    if (timeElapsed < duration) requestAnimationFrame(animation);
  }

  function easeInOutQuad(t, b, c, d) {
    t /= d / 2;
    if (t < 1) return (c / 2) * t * t + b;
    t--;
    return (-c / 2) * (t * (t - 2) - 1) + b;
  }

  requestAnimationFrame(animation);
}
const items = [
  {
    key: "1",
    label: <p className="lg:text-xl md:text-xl">Về ELEARNING</p>,
    children: (
      <div className="mt-10 text-left block grid-cols-3 lg:grid md:grid px-10 lg:px-56 ">
        <div className="col-span-2 flex flex-col justify-center">
          <p className="text-3xl mb-5 font-medium">
            Chúng tôi tin vào tiềm năng của con người
          </p>
          <p>
            CyberSoft được thành lập dựa trên niềm tin rằng bất cứ ai cũng có
            thể học lập trình. <br /> Bất kể ai cũng có thể là một lập trình,
            tham gia trong đội ngữ Tech, bất kể tuổi tác, nền tảng, giới tính
            hoặc tình trạng tài chính. Chúng tôi không bỏ qua những người mới
            bắt đầu hoặc chưa có kinh nghiệm theo đuổi đam mê lập trình. Thay
            vào đó, chúng tôi chào đón học viên của tất cả các cấp độ kinh
            nghiệm. Lộ trình học tập của CyberSoft may đo cho từng đối tượng để
            học xong và đi làm ngay.
          </p>
          <button
            onClick={() => {
              window.location.href = "/listcourse";
            }}
            className="font-bold mt-5 px-1 py-2 w-40 border-2 border-green-500/75  flex justify-center items-center rounded text-white bg-green-500 hover:brightness-75"
          >
            Tham Khảo Khóa Học
          </button>
        </div>
        <img
          className="col-span-1 hidden lg:block md:block"
          width={400}
          src={require("../../img/Elagi.jpg")}
          alt=""
        />
      </div>
    ),
  },
  {
    key: "2",
    label: <p className="lg:text-xl md:text-xl">HỌC TẬP</p>,
    children: (
      <div className="mt-10 flex justify-center items-center px-5 lg:px-60 ">
        <div className="flex flex-col justify-center items-center gap-5">
          <div className="rounded-full border-2 border-green-500/100 w-20 h-20 overflow-hidden">
            <img
              className="h-full object-cover"
              src={require("../../img/ntsang.jpg")}
              alt=""
            />
          </div>
          <p className="font-medium text-xl">Nguyễn Tấn Sang</p>
          <div>
            <p className="text-green-700 font-medium">CEO Elearning</p>
            <p>20 năm kinh nghiệm Code, Quản lý, Đào tạo</p>
          </div>
          <button
            onClick={() => {
              slowScroll(1800, 1000);
            }}
            className="font-bold mt-5 px-1 py-2 w-40 border-2 border-green-500/75  flex justify-center items-center rounded text-white bg-green-500 hover:brightness-75"
          >
            Lộ Trình Học
          </button>
        </div>
        <i className="ml-10">
          "Chúng ta tạo nên máy tính hệ thống như xây một thành phố vậy: trải
          qua thời gian, không có kế hoạch cụ thể, bắt đầu từ các phế tích.{" "}
          <br />
          Công việc của lập trình viên không phải là ngồi 8 tiếng để gõ code,
          công việc của lập trình viên là làm thay đổi thế giới làm cho nó tốt
          đẹp hơn"
        </i>
      </div>
    ),
  },
  {
    key: "3",
    label: <p className="lg:text-xl md:text-xl">CỰU SINH VIÊN</p>,
    children: (
      <div className="block gap-10 mt-10 lg:flex md:flex">
        <Card
          className="mt-5 shadow-2xl"
          hoverable
          style={{
            width: 240,
          }}
          cover={<img alt="example" src={require("../../img/hocvien1.jpg")} />}
        >
          <Meta
            title="Học viên chuyển nghành"
            description={
              <div>
                <p>Nguyễn Tấn Trọng</p>
                <p className="mt-5">
                  " Tôi vô cùng hài lòng với công việc hiện tại của tôi tại
                  GOOGLE. Tôi đã chọn lại được đúng đam mê của mình nhờ các khóa
                  học chuyên sâu tại Elearning."
                </p>
              </div>
            }
          />
        </Card>
        <Card
          className="mt-5 shadow-2xl"
          hoverable
          style={{
            width: 240,
          }}
          cover={<img alt="example" src={require("../../img/hocvien2.jpg")} />}
        >
          <Meta
            title="Học viên nghành CNTT"
            description={
              <div>
                <p>Lê Thị Bé Ba</p>
                <p className="mt-5">
                  " Tôi học được rất nhiều từ kỹ năng làm việc, các kỹ thuật
                  chuyên sâu và kỹ năng mềm tại các khóa học của Elearning. Điều
                  này đã giúp tôi tự tin rất nhiều khi làm việc tại tập đoàn
                  Facebook "
                </p>
              </div>
            }
          />
        </Card>
        <Card
          className="mt-5 shadow-2xl"
          hoverable
          style={{
            width: 240,
          }}
          cover={<img alt="example" src={require("../../img/hocvien3.jpg")} />}
        >
          <Meta
            title="Học viên chuyển nghành"
            description={
              <div>
                <p>Nguyễn Hiếu</p>
                <p className="mt-5">
                  " Sau 2 năm tốt nghiệp ngành xây dựng, tôi đã thấy mình không
                  phù hợp. Tôi đã tìm đến với Elearning và nay tôi đã có được
                  công việc đúng mong đợi của mình tại Youtube"
                </p>
              </div>
            }
          />
        </Card>
      </div>
    ),
  },
];
export default function TabsE() {
  return (
    <Bounce triggerOnce duration={2500}>
      <div className="flex justify-center items-center py-20 w-screen ">
        <Tabs
          className="flex justify-center items-center px-5  w-full lg:px-40 "
          defaultActiveKey="1"
          items={items}
          onChange={onChange}
        />
      </div>
    </Bounce>
  );
}
