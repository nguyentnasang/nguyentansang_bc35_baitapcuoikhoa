import React from "react";
import Lottie from "lottie-react";
import groovyWalkAnimation from "../../assets/90714-online-learning.json";
import MovingText from "react-moving-text";
import { Slide } from "react-awesome-reveal";
export default function SliderAnimate() {
  return (
    <div
      style={{ paddingTop: "6.5rem" }}
      className=" bg-white h-screen w-screen z-50 block grid-cols-2 shadow-xl shadow-green-200/50 lg:grid md:block m-auto"
    >
      <Slide
        duration={2000}
        triggerOnce
        direction="down"
        className="flex flex-col justify-center items-center"
      >
        <div className="flex flex-col justify-center items-center">
          <p className="text-5xl font-medium text-sky-900">Chào mừng đến với</p>
          <MovingText
            className="font-black text-6xl text-green-500"
            type="shadow"
            duration="1200ms"
            delay="0s"
            direction="normal"
            timing="ease"
            iteration="infinite"
            fillMode="none"
          >
            ELEARNING
          </MovingText>
        </div>
      </Slide>
      <Slide duration={2000} direction="down" triggerOnce>
        <Lottie animationData={groovyWalkAnimation} loop={true} />
      </Slide>
    </div>
  );
}
