import React, { useEffect, useState } from "react";
import { message, Space, Table, Tag } from "antd";
import { useParams } from "react-router-dom";
import {
  postDeleteCourse,
  postRegisCourseUser,
  postWaitCourse,
} from "./../../../service/courseService";
export default function MGRWaitCourse() {
  let param = useParams();
  const columns = [
    {
      title: "Mã Khóa Học",
      dataIndex: "maKhoaHoc",
      key: "maKhoaHoc",
    },
    {
      title: "Tên Khóa Học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Chờ Xác Thực",
      dataIndex: "action",
      key: "action",
    },
  ];
  const data = [
    {
      key: "1",
      name: "John Brown",
      age: 32,
      address: "New York No. 1 Lake Park",
      tags: ["nice", "developer"],
    },
    {
      key: "2",
      name: "Jim Green",
      age: 42,
      address: "London No. 1 Lake Park",
      tags: ["loser"],
    },
    {
      key: "3",
      name: "Joe Black",
      age: 32,
      address: "Sidney No. 1 Lake Park",
      tags: ["cool", "teacher"],
    },
  ];
  const [waitCourse, setwaitCourse] = useState([]);
  useEffect(() => {
    postWaitCourse({ taiKhoan: param.tk })
      .then((res) => {
        // console.log(res);
        setwaitCourse(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderWaitCourse = () => {
    return waitCourse.map((item) => {
      return {
        ...item,
        action: (
          <div>
            <button
              onClick={() => {
                postRegisCourseUser({
                  taiKhoan: param.tk,
                  maKhoaHoc: item.maKhoaHoc,
                })
                  .then((res) => {
                    message.success("ghi danh thành công");
                    setTimeout(() => {
                      window.location.reload();
                    }, 1000);
                    console.log(res);
                  })
                  .catch((err) => {
                    message.error("ghi danh thất bại");
                    console.log(err);
                  });
              }}
              className="border-2 border-black rounded-md px-2 py-1 mx-1 bg-green-500 hover:brightness-75"
            >
              xác thực
            </button>
            <button
              onClick={() => {
                postDeleteCourse({
                  taiKhoan: param.tk,
                  maKhoaHoc: item.maKhoaHoc,
                })
                  .then((res) => {
                    message.success("xóa thành công");
                    setTimeout(() => {
                      window.location.reload();
                    }, 1000);
                    console.log(res);
                  })
                  .catch((err) => {
                    message.error("xóa thất bại");
                    console.log(err);
                  });
              }}
              className="border-2 border-black rounded-md px-2 py-1 mx-1 bg-red-500 hover:brightness-75"
            >
              hủy
            </button>
          </div>
        ),
      };
    });
  };
  return (
    <div>
      <p>KHÓA HỌC CHỜ XÁC THỰC</p>
      <Table columns={columns} dataSource={renderWaitCourse()} />;
    </div>
  );
}
