import React from "react";
import Background from "../../img/bgevent.png";
export default function EventPage() {
  return (
    <div className="overflow-hidden">
      <div
        style={{ backgroundImage: `url("${Background}")` }}
        className="bg-cover bg-right bg-fixed h-screen w-screen flex flex-col justify-center items-center px-3"
      >
        {/* <img src={require("../../img/bgevent.png")} alt="" /> */}
        <p className="text-3xl font-bold text-white italic">
          Khám phá, vui chơi, sáng tạo!
        </p>
        <p className="text-4xl font-bold text-white mt-5">
          Cùng nhau khám phá những điều mới mẻ
        </p>
      </div>
      <div className="my-10 px-10 lg:px-40 md:px-20">
        <p className="text-2xl font-bold text-green-700 mb-5">
          Hãy tham gia với chúng tớ
        </p>
        <p className="text-justify text-xl">
          Một phong trào với hơn 1.180.000 học sinh khắp cả nước tham gia. Đã có
          61,437 sự kiện được đăng ký cho năm 2022, tại TPHCM. 1,589,446 giờ lập
          trình đã được thực hiện. Giờ lập trình diễn ra quanh năm, nhưng vào
          tháng 12 hàng năm, lớp của bạn có thể cùng hàng nghàn sinh viên trên
          khắp cả nước kỷ niệm Tuần lễ Giáo dục Khoa học Máy tính với Giờ lập
          trình. Hãy đăng ký tham gia lễ kỷ niệm thường niên bắt đầu vào tháng
          Mười hàng năm.
        </p>
      </div>
      <div className=" grid-cols-3 gap-5 px-10  pb-28 lg:grid lg:px-40 md:grid md:px-20">
        <div className="flex justify-center items-center flex-col">
          <img className="w-52" src={require("../../img/map.png")} alt="" />
          <p className="text-xl mt-5">Học sinh đang học bằng hơn 45 ngôn ngữ</p>
        </div>
        <div className="flex justify-center items-center flex-col">
          <img className="w-52" src={require("../../img/map2.png")} alt="" />
          <p className="text-xl mt-5">
            Hơn 100 triệu học sinh đã thử học Giờ lập trình
          </p>
        </div>
        <div className="flex justify-center items-center flex-col">
          <img className="w-52" src={require("../../img/mapgirl.png")} alt="" />
          <p className="text-xl mt-5">
            Nhiều học sinh nữ đã được làm quen với Khoa học máy tính hơn so với
            70 năm qua
          </p>
        </div>
      </div>
      <div className="px-5 lg:px-32">
        <p className="text-2xl font-bold text-green-700 mb-5">
          Nổi bật của sự kiện
        </p>
        <div className="grid grid-cols-3 gap-5">
          <img
            className="w-full h-full"
            src={require("../../img/hocsinhlt1.png")}
            alt=""
          />
          <img
            className="w-full h-full"
            src={require("../../img/hocsinhlt2.png")}
            alt=""
          />
          <img
            className="w-full h-full"
            src={require("../../img/hocsinhlt3.png")}
            alt=""
          />
          <img
            className="w-full h-full"
            src={require("../../img/hocsinhlt4.png")}
            alt=""
          />
          <img
            className="w-full col-span-2 row-span-2"
            src={require("../../img/hocsinh6.png")}
            alt=""
          />
          <img
            className="w-full h-full"
            src={require("../../img/hocsinhlt5.png")}
            alt=""
          />
        </div>
      </div>
    </div>
  );
}
