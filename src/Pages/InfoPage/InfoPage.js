import React from "react";
import Background from "../../img/bginfo4.png";

export default function InfoPage() {
  return (
    <div className="overflow-hidden">
      <div className="">a</div>
      <div
        style={{ backgroundImage: `url("${Background}")` }}
        className="h-screen w-screen bg-cover bg-center flex justify-center items-center flex-col"
      >
        <p className="text-4xl font-bold text-white">
          Elearning Cộng Đồng Lập Trình
        </p>
        <p className="text-2xl font-medium text-white mt-5">
          Cùng Nhau Khám Phá Những Điều Mới Mẻ
        </p>
      </div>
      <div className="pt-20">
        <div className=" grid-cols-2 items-center lg:grid md:grid">
          <div className=" px-10 text-justify">
            <div className="">
              <p className="text-left text-xl font-bold text-green-700">
                ELEARNING?
              </p>
            </div>
            <p className="text-3xl my-3 font-bold">Nơi hội tụ kiến thức</p>
            <p>
              Đây là nền tảng giảng dạy và học tập trực tuyến được xây dựng để
              hỗ trợ phát triển giáo dục trong thời đại công nghiệp 4.0, được
              xây dựng dựa trên cơ sở quan sát toàn bộ các nhu cầu cần thiết của
              một lớp học offline. Từ đó đáp ứng và đảm bảo cung cấp các công cụ
              toàn diện, dễ sử dụng cho giáo viên và học sinh, giúp tạo nên một
              lớp học trực tuyến thú vị và hấp dẫn.
            </p>
          </div>
          <img src={require("../../img/tt1.png")} alt="" />
        </div>
      </div>
      <div>
        <div className="mt-10 flex flex-col-reverse grid-cols-2 items-center lg:grid md:grid">
          <img src={require("../../img/tt2.png")} alt="" />
          <div className=" px-10 text-justify">
            <div className="">
              <p className="text-left text-xl font-bold text-green-700">
                Chương Trình Học ELEARNING?
              </p>
            </div>
            <p className="text-3xl my-3 font-bold">Hệ thống học tập hàng đầu</p>
            <p>
              Giảng viên đều là các chuyên viên thiết kế đồ họa và giảng viên
              của các trường đại học danh tiếng trong thành phố: Đại học CNTT,
              KHTN, Bách Khoa,…Trang thiết bị phục vụ học tập đầy đủ, phòng học
              máy lạnh, màn hình LCD, máy cấu hình mạnh, mỗi học viên thực hành
              trên một máy riêng.100% các buổi học đều là thực hành trên máy
              tính. Đào tạo với tiêu chí: “học để làm được việc”, mang lại cho
              học viên những kiến thức hữu ích nhất, sát với thực tế nhất.
            </p>
          </div>
        </div>
      </div>
      <div>
        <div className=" grid-cols-2 items-center lg:grid md:grid">
          <div className=" px-10 text-justify">
            <div className="">
              <p className="text-left text-xl font-bold text-green-700">
                TẦM NHÌN V LEARNING
              </p>
            </div>
            <p className="text-3xl my-3 font-bold">
              Đào tạo lập trình chuyên sâu
            </p>
            <p>
              Trở thành hệ thống đào tạo lập trình chuyên sâu theo nghề hàng đầu
              khu vực, cung cấp nhân lực có tay nghề cao, chuyên môn sâu cho sự
              phát triển công nghiệp phần mềm trong thời đại công nghệ số hiện
              nay, góp phần giúp sự phát triển của xã hội, đưa Việt Nam thành
              cường quốc về phát triển phần mềm.giúp người học phát triển cả tư
              duy, phân tích, chuyên sâu nghề nghiệp, học tập suốt đời, sẵn sàng
              đáp ứng mọi nhu cầu của doanh nghiệp.
            </p>
          </div>
          <img src={require("../../img/tt3.png")} alt="" />
        </div>
      </div>
      <div>
        <div className="mt-10 flex flex-col-reverse grid-cols-2 items-center lg:grid md:grid">
          <img src={require("../../img/tt4.png")} alt="" />
          <div className=" px-10 text-justify">
            <div className="">
              <p className="text-left text-xl font-bold text-green-700">
                SỨ MỆNH V LEARNING
              </p>
            </div>
            <p className="text-3xl my-3 font-bold">
              Phương pháp đào tạo hiện đại
            </p>
            <p>
              Sử dụng các phương pháp đào tạo tiên tiến và hiện đại trên nền
              tảng công nghệ giáo dục, kết hợp phương pháp truyền thống, phương
              pháp trực tuyến, lớp học đảo ngược và học tập dựa trên dự án thực
              tế, phối hợp giữa đội ngũ training nhiều kinh nghiệm và yêu cầu từ
              các công ty, doanh nghiệp. Qua đó, V learning giúp người học phát
              triển cả tư duy, phân tích, chuyên sâu nghề nghiệp, học tập suốt
              đời, sẵn sàng đáp ứng mọi nhu cầu của doanh nghiệp.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
