import React from "react";
import { Button, Form, Input, message } from "antd";
import { postLogin } from "./../../service/userService";
// import { useNavigate } from "react-router-dom";
import { userLocalService } from "../../service/localService";
import Background from "../../img/login2.1.png";
import { Slide } from "react-awesome-reveal";
import { useDispatch } from "react-redux";
import { setLoadingOff } from "../../redux-toolkit/loadingSlice";
export default function LoginPage() {
  // const navigate = useNavigate();
  let dispatch = useDispatch();
  dispatch(setLoadingOff());

  const onFinish = (values) => {
    console.log("Success:", values);
    postLogin(values)
      .then((res) => {
        userLocalService.set(res.data);
        console.log("res", res.data.maLoaiNguoiDung);
        message.success("Đăng nhập thành công");
        setTimeout(() => {
          if (res.data.maLoaiNguoiDung === "HV") {
            window.location.href = "/";
          } else {
            window.location.href = "/admin";
          }
        }, 1000);
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        message.error("Đăng nhập thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className=" h-screen lg:grid md:grid lg:grid-cols-3 md:grid-cols-2">
      <div
        className="bg-center bg-cover text-white flex flex-col justify-center items-center py-10 md:col-span-1 lg:col-span-2 "
        style={{ backgroundImage: `url(${Background})` }}
        height={"100%"}
      >
        <p className=" brightness-100 text-6xl font-semibold flex">
          <Slide triggerOnce duration={2000} direction="left" className="mr-3">
            <p>XIN </p>
          </Slide>
          <Slide triggerOnce duration={2000} direction="right">
            <p> CHÀO</p>
          </Slide>
          <Slide triggerOnce duration={2000} delay={1000} direction="down">
            <p>!</p>
          </Slide>
        </p>
        <p className="my-5 text-2xl">
          Nếu chưa có tài khoản hãy đăng ký <br /> để tham gia cùng chúng tôi
        </p>
        <button
          onClick={() => {
            window.location.href = "/regis";
          }}
          className="bg-green-800 py-2 px-4 rounded-md text-xl"
        >
          Đăng ký
        </button>
      </div>
      <Form
        className="mx-10 pt-10 my-auto"
        layout="vertical"
        name="basic"
        // labelCol={{
        //   span: 8,
        // }}
        // wrapperCol={{
        //   span: 16,
        // }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <p className="font-bold text-4xl">ĐĂNG NHẬP</p>
        <p className="mt-3 mb-5 italic">admin có thể đăng nhập tại đây</p>
        <Form.Item
          label={<p className="text-xs">TÀI KHOẢN</p>}
          name="taiKhoan"
          rules={[
            {
              // required: true,
              message: "Vui lòng nhập tài khoản",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label={<p className="text-xs">MẬT KHẨU</p>}
          name="matKhau"
          rules={[
            {
              // required: true,
              message: "Vui lòng nhập mật khẩu",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          // wrapperCol={{
          //   offset: 8,
          //   span: 16,
          // }}
        ></Form.Item>

        <Form.Item
        // wrapperCol={{
        //   offset: 8,
        //   span: 16,
        // }}
        >
          <a className="block mb-3 text-green-700" href="">
            Quên mật khẩu?
          </a>
          <button
            className="bg-green-700 px-2 py-1 rounded-md text-yellow-50 hover:brightness-75 mb-5"
            htmlType="submit"
          >
            Đăng Nhập
          </button>
          <div class="flex items-center my-4 before:flex-1 before:border-t before:border-gray-300 before:mt-0.5 after:flex-1 after:border-t after:border-gray-300 after:mt-0.5">
            <p class="text-center font-semibold mx-4 mb-0 flex">
              <span>E</span>

              <span>L</span>

              <span>E</span>

              <span>A</span>

              <span>R</span>

              <span>N</span>

              <span>I</span>

              <span>N</span>

              <span>G</span>
            </p>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}
