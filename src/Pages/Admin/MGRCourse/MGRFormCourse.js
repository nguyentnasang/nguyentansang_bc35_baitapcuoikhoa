import React from "react";
import { InboxOutlined, UploadOutlined } from "@ant-design/icons";
import moment from "moment";
import {
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Rate,
  Select,
  Upload,
} from "antd";
import { postNewCourse } from "../../../service/courseService";
import { getCourse } from "../../../service/courseService";

import { useDispatch } from "react-redux";
import { setLoadingOff } from "../../../redux-toolkit/loadingSlice";
export default function MGRFormCourse() {
  let dispatch = useDispatch();
  dispatch(setLoadingOff());
  const { Option } = Select;
  const formItemLayout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 14,
    },
  };
  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList;
  };
  const onFinish = (values) => {
    const dataArr = {
      ...values,
      ngayTao: moment(values.ngayTao).format("DD/MM/YYYY"),
      hinhAnh: values.hinhAnh[0].name,
    };

    postNewCourse(dataArr)
      .then((res) => {
        message.success("thêm thành công");
        getCourse().then((res) => {
          console.log("result", res);
        });
        console.log(res);
      })
      .catch((err) => {
        message.error("khong thanh cong");
        console.log(err);
      });
  };
  const handleChangeDatePicker = (value) => {};
  const config = {
    rules: [
      {
        type: "object",
        required: true,
        message: "Please select time!",
      },
    ],
  };
  return (
    <div>
      {" "}
      <Form
        name="validate_other"
        {...formItemLayout}
        onFinish={onFinish}
        initialValues={{
          "input-number": 3,
          "checkbox-group": ["A", "B"],
          rate: 3.5,
        }}
        style={{
          maxWidth: 600,
        }}
      >
        <Form.Item
          label="Mã KHóa Học"
          name="maKhoaHoc"
          key="maKhoaHoc"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Bí Danh"
          name="biDanh"
          key="biDanh"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Tên Khóa Học"
          name="tenKhoaHoc"
          key="tenKhoaHoc"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="Lượt Xem">
          <Form.Item name="luotXem" noStyle>
            <InputNumber min={0} max={10000000000} />
          </Form.Item>
          <span
            className="ant-form-text"
            style={{
              marginLeft: 8,
            }}
          ></span>
        </Form.Item>
        <Form.Item className="bg-green-200" name="danhGia" label="Đánh Giá">
          <Rate />
        </Form.Item>
        <Form.Item
          name="hinhAnh"
          label="Hình Ảnh"
          valuePropName="fileList"
          getValueFromEvent={normFile}
          extra="longgggggggggggggggggggggggggggggggggg"
        >
          <Upload
            name="logo"
            action="/upload.do"
            beforeUpload={() => false}
            listType="picture"
          >
            <Button icon={<UploadOutlined />}>Click to upload</Button>
          </Upload>
        </Form.Item>
        <Form.Item
          name="maNhom"
          label="Mã Nhóm"
          hasFeedback
          rules={[
            {
              required: true,
              message: "Please select your country!",
            },
          ]}
        >
          <Select placeholder="Please select a country">
            <Option value="GP01">GP01</Option>
          </Select>
        </Form.Item>
        <Form.Item name="ngayTao" label="Ngày Tạo">
          <DatePicker format={"DD/MM/YYYY"} onChange={handleChangeDatePicker} />
        </Form.Item>
        <Form.Item
          name="maDanhMucKhoaHoc"
          label="Mã Danh Mục"
          hasFeedback
          rules={[
            {
              required: true,
              message: "Please select your country!",
            },
          ]}
        >
          <Select placeholder="Please select a country">
            <Option value="BackEnd">Lập trình Backend</Option>
            <Option value="Design">Thiết kế Web</Option>
            <Option value="DiDong">Lập trình di động</Option>
            <Option value="FrontEnd">Lập trình Front end</Option>
            <Option value="FullStack">Lập trình Full Stack</Option>
            <Option value="TuDuy">Tư duy lập trình</Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="Tài Khoản Tạo"
          name="taiKhoanNguoiTao"
          key="maKhoaHoc"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Mô Tả"
          name="moTa"
          key="moTa"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            span: 12,
            offset: 6,
          }}
        >
          <Button className="bg-green-500" htmlType="submit">
            thêm
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
