import React from "react";
import MGRNotRegisUser from "./MGRNotRegisUser";
import MGRReadyUser from "./MGRReadyUser";
import MGRWaitUser from "./MGRWaitUser";

export default function MGRRegisUser() {
  return (
    <div>
      <MGRNotRegisUser />
      <MGRWaitUser />
      <MGRReadyUser />
    </div>
  );
}
