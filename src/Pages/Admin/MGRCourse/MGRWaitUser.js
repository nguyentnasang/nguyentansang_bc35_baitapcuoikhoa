import React, { useEffect, useState } from "react";
import { message, Space, Table, Tag } from "antd";
import { postWaitUser } from "../../../service/userService";
import { useParams } from "react-router-dom";
import {
  postDeleteCourse,
  postRegisCourseUser,
} from "../../../service/courseService";
export default function MGRWaitUser() {
  const columns = [
    {
      title: "Tài Khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Họ Tên",
      dataIndex: "hoTen",
      key: "hoTen",
    },
    {
      title: "Chờ xác nhận",
      dataIndex: "action",
      key: "action",
    },
  ];
  const data = [
    {
      key: "1",
      name: "John Brown",
      age: 32,
      address: "New York No. 1 Lake Park",
      tags: ["nice", "developer"],
    },
    {
      key: "2",
      name: "Jim Green",
      age: 42,
      address: "London No. 1 Lake Park",
      tags: ["loser"],
    },
    {
      key: "3",
      name: "Joe Black",
      age: 32,
      address: "Sidney No. 1 Lake Park",
      tags: ["cool", "teacher"],
    },
  ];
  let param = useParams();
  const [WaitUser, setWaitUser] = useState([]);
  const dataWaitUser = () => {
    return WaitUser.map((item) => {
      console.log("item", item);
      return {
        ...item,
        action: (
          <div>
            <button
              onClick={() => {
                postRegisCourseUser({
                  taiKhoan: item.taiKhoan,
                  maKhoaHoc: param.tk,
                })
                  .then((res) => {
                    message.success("ghi danh thành công");
                    setTimeout(() => {
                      window.location.reload();
                    }, 1000);
                    console.log(res);
                  })
                  .catch((err) => {
                    message.error("ghi danh thất bại");
                    console.log(err);
                  });
              }}
              className="border-2 border-black m-1 rounded-md px-2 py-1"
            >
              xác thực
            </button>
            <button
              onClick={() => {
                postDeleteCourse({
                  taiKhoan: item.taiKhoan,
                  maKhoaHoc: param.tk,
                })
                  .then((res) => {
                    console.log(res);
                    message.success("hủy thành công");
                    setTimeout(() => {
                      window.location.reload();
                    }, 1000);
                  })
                  .catch((err) => {
                    message.error("hủy thất bại");
                    console.log(err);
                  });
              }}
              className="border-2 border-black m-1 rounded-md px-2 py-1"
            >
              hủy
            </button>
          </div>
        ),
      };
    });
  };
  useEffect(() => {
    postWaitUser({ maKhoaHoc: param.tk })
      .then((res) => {
        // console.log(res);
        setWaitUser(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <p>HỌC VIÊN CHỜ XÁC THỰC</p>
      <Table columns={columns} dataSource={dataWaitUser()} />;
    </div>
  );
}
