import React from "react";
import { Dropdown, Space, Button } from "antd";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";
const items = [
  {
    key: "4",
    danger: false,
    label: (
      <p
        className="bg-green-500 text-white h-full px-2 py-1 rounded-md hover:brightness-75"
        onClick={() => {
          window.location.href = "/userinfo";
        }}
      >
        CẬP NHẬT THÔNG TIN
      </p>
    ),
  },
  {
    key: "4",
    danger: false,
    label: (
      <p
        className="bg-red-500 text-white h-full px-2 py-1 rounded-md hover:brightness-75"
        onClick={() => {
          userLocalService.remote();
          window.location.href = "/";
        }}
      >
        ĐĂNG XUẤT
      </p>
    ),
  },
];
export default function Navbar() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  const renderHeader = () => {
    if (user) {
      return (
        <Dropdown
          className="shadow-2xl"
          menu={{
            items,
          }}
        >
          <a onClick={(e) => e.preventDefault()}>
            <Space>
              <img
                src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp"
                class="rounded-full w-16"
                alt="Avatar"
              />
            </Space>
          </a>
        </Dropdown>
      );
    } else {
      return (
        <div>
          <Button
            className="bg-green-400 text-white font-medium"
            onClick={() => {
              window.location.href = "/login";
            }}
          >
            ĐĂNG NHẬP
          </Button>
        </div>
      );
    }
  };
  return (
    <div className="py-3 flex justify-center items-center">
      {renderHeader()}
    </div>
  );
}
