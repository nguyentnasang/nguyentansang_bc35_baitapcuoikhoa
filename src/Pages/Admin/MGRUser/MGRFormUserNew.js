import React from "react";
import { useDispatch } from "react-redux";
import { setLoadingOff } from "../../../redux-toolkit/loadingSlice";
import { Button, Form, Input, message, Select } from "antd";
import { Option } from "antd/es/mentions";
import { postAddUser } from "../../../service/userService";
import { LeftOutlined } from "@ant-design/icons";
export default function MGRFormUserNew() {
  let dispatch = useDispatch();
  dispatch(setLoadingOff());
  const onFinish = (values) => {
    // console.log("Success:", values);

    postAddUser(values)
      .then((res) => {
        message.success("thêm người dùng thành công");

        console.log(res);
      })
      .catch((err) => {
        message.error(err.response.data);
        console.log(err.response.data);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div>
      {" "}
      <Form
        className=""
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="tài Khoản"
          name="taiKhoan"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="mật khẩu"
          name="matKhau"
          rules={[
            {
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="họ tên"
          name="hoTen"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="số điện thoại"
          name="soDT"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="maLoaiNguoiDung"
          label="mã loại người dùng"
          rules={[{ required: true }]}
        >
          <Select
            placeholder="chọn loại người dùng"
            // onChange={onGenderChange}
            allowClear
          >
            <Option value="HV">Học Viên</Option>
            <Option value="GV">Giáo Vụ</Option>
          </Select>
        </Form.Item>
        <Form.Item name="maNhom" label="mã nhóm" rules={[{ required: true }]}>
          <Select
            placeholder="chọn nhóm người dùng"
            // onChange={onGenderChange}
            allowClear
          >
            <Option value="GP06">nhóm 6</Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="email"
          name="email"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        ></Form.Item>
        <a className="flex justify-center items-center" href="/admin">
          <LeftOutlined />
          <LeftOutlined />
          trở lại
        </a>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button className="bg-green-500" htmlType="submit">
            Thêm
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
