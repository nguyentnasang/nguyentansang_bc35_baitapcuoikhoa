import React, { useState } from "react";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";
import UserListPage from "./MGRUser/UserListPage";
import CourseListPage from "./MGRCourse/CourseListPage";
const { Header, Sider, Content } = Layout;
export default function Admin() {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const [ContentArr, setContentArr] = useState(<UserListPage />);
  return (
    <div>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            onClick={(item) => {
              if (item.key == 1) {
                setContentArr(<UserListPage />);
              } else {
                setContentArr(<CourseListPage />);
              }
            }}
            theme="dark"
            mode="inline"
            defaultSelectedKeys={["1"]}
            items={[
              {
                key: "1",
                icon: <UserOutlined />,
                label: "Quản lý người dùng",
              },
              {
                key: "2",
                icon: <VideoCameraOutlined />,
                label: "Quản lý khóa học",
              },
            ]}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="text-left text-2xl"
            style={{
              padding: 0,
              background: colorBgContainer,
            }}
          >
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: () => setCollapsed(!collapsed),
              }
            )}
          </Header>
          <Content
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
              background: colorBgContainer,
            }}
          >
            {ContentArr}
          </Content>
        </Layout>
      </Layout>
    </div>
  );
}
