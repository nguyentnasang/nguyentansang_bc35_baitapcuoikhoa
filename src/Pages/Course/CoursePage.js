import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import {
  getCourseByCate,
  postRegisCourse,
} from "./../../service/courseService";
import { Button, Card, message } from "antd";
import { useSelector } from "react-redux";
//DANH MỤC KHÓA HỌC
export default function CoursePage() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  let params = useParams();
  const [courseArr, setcourseArr] = useState([]);
  useEffect(() => {
    getCourseByCate(params.id)
      .then((res) => {
        setcourseArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderCardCourse = () => {
    return courseArr?.map((item) => {
      let axiosArr = {
        maKhoaHoc: item.maKhoaHoc,
        taiKhoan: user?.taiKhoan,
      };
      return (
        <Card
          className="shadow-2xl border-2 mt-5"
          hoverable
          cover={
            <img
              className="h-48 object-cover"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <p className="text-xl font-medium">{item.tenKhoaHoc}</p>

          <div>
            <div className="my-3">
              <p className="line-through italic">800.000đ</p>
              <p className="text-xl font-medium text-green-500">400.000đ</p>
            </div>
            <NavLink to={`/detail/${item.maKhoaHoc}`}>
              <button className="bg-green-400 mx-2 my-1 px-2 py-1 rounded-md text-white font-medium hover:brightness-75">
                xem chi tiết
              </button>
            </NavLink>
            <button
              className="bg-green-400 mx-2 my-1 px-2 py-1 rounded-md text-white font-medium hover:brightness-75"
              onClick={() => {
                postRegisCourse(axiosArr)
                  .then((res) => {
                    console.log(res);
                    message.success("Đăng ký thành công");
                  })
                  .catch((err) => {
                    console.log(err);
                    message.warning(err.request.response);
                  });
              }}
            >
              Đăng ký
            </button>
          </div>
        </Card>
      );
    });
  };
  const renderNameCourse = () => {
    if (params.id == "BackEnd") {
      return (
        <p className="pt-32 text-3xl font-bold text-green-800">
          Các Khóa Học Lập Trình Backend
        </p>
      );
    } else if (params.id == "Design") {
      return (
        <p className="pt-32 text-3xl font-bold text-green-800">
          Các Khóa Học Thiết Kế Website
        </p>
      );
    } else if (params.id == "DiDong") {
      return (
        <p className="pt-32 text-3xl font-bold text-green-800">
          Các Khóa Học Lập Trình Di Động
        </p>
      );
    } else if (params.id == "FrontEnd") {
      return (
        <p className="pt-32 text-3xl font-bold text-green-800">
          Các Khóa Học Lập Trình Fontend
        </p>
      );
    } else if (params.id == "FullStack") {
      return (
        <p className="pt-32 text-3xl font-bold text-green-800">
          Các Khóa Học Lập Trình Full Stack
        </p>
      );
    } else if (params.id == "TuDuy") {
      return (
        <p className="pt-32 text-3xl font-bold text-green-800">
          Các Khóa Học Về Tư Duy Lập Trình
        </p>
      );
    }
  };
  return (
    <div>
      {renderNameCourse()}
      <div className="gap-5 px-20 pt-10 lg:grid lg:grid-cols-4 md:grid md:px-10 md:grid-cols-3">
        {renderCardCourse()}
      </div>
    </div>
  );
}
